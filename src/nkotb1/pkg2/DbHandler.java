package nkotb1.pkg2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Scanner;

public class DbHandler {

    String url = "jdbc:mysql://localhost:3306/nkotb2";
    String user = "root";
    String password = "1337";
    Connection conn = null;

    Scanner userInput = new Scanner(System.in);
    Statement stmt = null;
    ResultSet rs = null;

    public Connection dbConnection() {
        try {
            //1. Anslutning till mysql
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException exc) {
            System.out.println(exc.getMessage());
        }
        return conn;
    }

    public ResultSet executeQuery(String query) throws SQLException {
        stmt = dbConnection().createStatement();
        rs = stmt.executeQuery(query);
        return rs;
    }

    //Metod för att jämföra användarinput mot databas för att se så logininformation stämmer.
    public boolean query(String username, String password) throws SQLException {
        String dbName = "";
        String dbPassword = "";
        stmt = dbConnection().createStatement();
        ResultSet rs = stmt.executeQuery("SELECT * FROM customers WHERE Firstname ='" + username + "' AND Password='" + password + "' ; ");
        while (rs.next()) {
            dbName = rs.getString("Firstname");
            dbPassword = rs.getString("Password");
        }
        if (dbName.equals(username) && dbPassword.equals(password)) {
            return true;
        } else {
            return false;
        }
    }
}
