package nkotb1.pkg2;

import java.sql.*;

public class AccountRepository {

    private DbHandler dbHandler;

    public AccountRepository(DbHandler dbHandler) {
        this.dbHandler = dbHandler;
    }

    /*public Account getAccount(String accountNumber) {
        Integer dbBalance = null;
        try {
            dbHandler.stmt = dbHandler.dbConnection().createStatement();
            ResultSet rs = dbHandler.stmt.executeQuery("SELECT * FROM accounts WHERE AccountID = '" + accountNumber + "'");
            while (rs.next()) {
                dbBalance = rs.getInt("Balance");
            }
        } catch (SQLException e) { }
        Account account = new Account(accountNumber, dbBalance);

        return account;
    }*/

    /*public void save(Account account) {
        try {
            dbHandler.stmt = dbHandler.dbConnection().createStatement();
            dbHandler.stmt.executeUpdate("UPDATE accounts SET Balance = '" + account.getBalance() + "' WHERE AccountID = '" + account.getAccountNumber() + "' ; ");

            } catch (SQLException e) {
        }

    }*/
    
    public Account getAccount(String accNr) {
        
        Account account = null;
        try {
            dbHandler.stmt = dbHandler.dbConnection().createStatement();
            ResultSet rs = dbHandler.stmt.executeQuery("SELECT * FROM accounts WHERE AccountID = '" + accNr + "'");
            
            while (rs.next()) {
                
                if (rs.getString("AccountID").equalsIgnoreCase(accNr)) {
                   
                    account = new Account(rs.getString("AccountID"), rs.getInt("balance"));
                }
            }
        } catch(SQLException e) { }
        
        return account;
    }
    
     public void save(String accNr, int balance) {
        try {
            dbHandler.stmt = dbHandler.dbConnection().createStatement();
            dbHandler.stmt.executeUpdate("UPDATE accounts SET Balance = '" + balance + "' WHERE AccountID = '" + accNr + "' ; ");

            } catch (SQLException e) {
        }

    }

}
