package nkotb1.pkg2;

import java.sql.SQLException;
import java.util.Scanner;

public class Menu {

    public void menuChoice(DbHandler dbhandler) throws SQLException {

        while (true) {
            Login login = new Login();
            Scanner input = new Scanner(System.in);
            User user = login.userInput(input);
            AccountRepository accountRepository = new AccountRepository(dbhandler);
            AccountService accService = new AccountService(accountRepository);

            if (!dbhandler.query(user.getUsername(), user.getPassword())) {
                System.out.println("Invalid username or password.");
                break;
            }
            user.userData(dbhandler);

            System.out.println("A: Online bank.");

            String userInput = input.next();

            if (userInput.equals("A")) {

                System.out.println("A: Show accounts.");
                System.out.println("B: Show loans.");
                System.out.println("C: Transaction history.");
                System.out.println("D: Transfer between personal accounts.");
                System.out.println("E: Transfer to other account.");
                String userInput1 = input.next();

                Transfer transfer = new Transfer(accService);

                switch (userInput1) {
                    case "A":
                        System.out.println("Not yet available.");
                        break;
                    case "B":
                        System.out.println("Not yet available.");
                        break;
                    case "C":
                        System.out.println("A: Show deposits.");
                        System.out.println("B: Show withdrawals.");
                        System.out.println("C: Show transfers.");

                        switch (userInput) {
                            case "A":
                                System.out.println("Not yet available.");
                                break;
                            case "B":
                                System.out.println("Not yet available.");
                                break;
                            case "C":
                                System.out.println("Not yet available.");
                                break;
                        }
                        break;
                    case "D":
                        System.out.println("Not yet available.");
                        break;

                    case "E":
                        user.listAccounts();
                        transfer.otherTransfer();
                        break;

                }

            }
        }
    }

}
