package nkotb1.pkg2;

import java.util.ArrayList;
import java.sql.*;

public class User {

    private int dbId;
    private String username;
    private String password;
    private ArrayList<Account> accounts;
    
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void showDescription() {
        System.out.println("Welcome, " + username + ".");
    }

    public int getDbId() {
        return dbId;
    }

    public void setDbId(int dbId) {
        this.dbId = dbId;
    }

    public ArrayList<Account> getAccount() {
        return accounts;
    }

    public void listAccounts() {
        for (Account account : accounts) {
            System.out.println("Account nr: " + account.getAccountNumber() + ", Balance: " + account.getBalance());
        }
    }

    //Arraylist som hämtar användarinfo från databas.
    public void userData(DbHandler db) throws SQLException {
        accounts = new ArrayList<>();
        ResultSet r = db.executeQuery("SELECT * FROM customers LEFT JOIN accounts ON CustomerID = accounts.customer WHERE firstName = '" + getUsername() + "';");

        while (r.next()) {
            accounts.add(new Account(r.getString("AccountID"), r.getInt("Balance")));
        }

    }

}
