package nkotb1.pkg2;

import java.util.Scanner;

public class Transfer {

    Scanner input = new Scanner(System.in);
    AccountService accountService;

    public Transfer (AccountService accountService){
        this.accountService = accountService;
    }
    //Denna ska föra över pengar till det angivna kontot.
    /*public void otherTransfer(User user) {

        try {
            System.out.println("Enter accountnumber to transfer from: \n");
            for (Account account : user.getAccount()) {
                System.out.println("Account nr: " + account.getAccountNumber() + ", Balance: " + account.getBalance());
            }
            String fromAccountNumber = input.next();
            Account sender = accountService.getAccount(fromAccountNumber);
            System.out.println("Enter accountnumber to transfer to: ");
            String accountNumber = input.next();
            Account reciever = accountService.getAccount(accountNumber);

            System.out.println("Enter ammount to transfer:");
            int ammount = input.nextInt();

            if (ammount <= sender.getBalance()) {
                reciever.setBalance(reciever.getBalance() + ammount);
                sender.setBalance(sender.getBalance() - ammount);
                System.out.println("After transfer.\n" + "Balance: " + sender.getBalance());
                accountService.save(reciever);
                accountService.save(sender);

            } else {
                System.out.println("Insufficient funds.");

            }
        } catch (Exception e) {
            System.out.println("You have entered invalid information.");
        }

    }*/
    
    public void otherTransfer() {
        String from = "";
        do {
            System.out.println("Enter account number to transfer from: \n");
            from = input.next();
        } while (!accountService.accountExists(from));
        
        String to = "";
        do {
            System.out.println("Enter account number to transfer to: \n");
            to = input.next();
        } while (!accountService.accountExists(to));
        
        System.out.println("Enter amount to transfer: ");
        int amount = input.nextInt();
        try {
            accountService.transfer(from, to, amount);
        }
        catch(InsufficientFundsException e) {
            System.out.println( e.getMessage() );
            
        }
        
    }

}
