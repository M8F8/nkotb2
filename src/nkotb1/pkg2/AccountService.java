package nkotb1.pkg2;


public class AccountService {
    
    private AccountRepository accountRepository;
    
    public AccountService(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    
    }
    
    public void transfer(String from, String to, int amount) throws InsufficientFundsException{
        Account sender = accountRepository.getAccount(from);
        Account receiver = accountRepository.getAccount(to);
        
        if (amount <= sender.getBalance()) {

            accountRepository.save(from, sender.getBalance() - amount);
            accountRepository.save(to, receiver.getBalance() + amount);
            
        } else {
            throw new InsufficientFundsException("Insufficient funds."); 
        }
    
    }
    
    public boolean accountExists(String accNr) {

        if (accountRepository.getAccount(accNr) != null) {
            return true;
        }
        else {
            return false;
        }
    }
}
