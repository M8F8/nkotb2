package nkotb1.pkg2;

import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;

public class Account {

    private String accountNumber;
    private int balance;
    
    public Account() {
    
    }
    
    public Account( String accountNumber, int balance ) {
        this.accountNumber = accountNumber;
        this.balance = balance;
    }
    
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int personal) {
        this.balance = personal;
    }
}
