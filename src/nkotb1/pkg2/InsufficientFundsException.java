package nkotb1.pkg2;

public class InsufficientFundsException extends Exception{
    public  InsufficientFundsException(String message) {
        super(message);
    }

    public InsufficientFundsException(String message, Throwable throwable) {
        super(message, throwable);
    }
}
